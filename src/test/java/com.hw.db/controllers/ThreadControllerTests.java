package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.mockito.internal.verification.Times;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ThreadControllerTests {
    private Thread thr;
    private String slug = "mem";
    private String id = "1337";

    @BeforeEach
    @DisplayName("Test the thread")
    void thread() {
        thr = new Thread("thread", new Timestamp(0), "frm", "msg", slug, "hello world", 43);
    }

    @Test
    @DisplayName("Check thread by ID")
    void checkThreadById() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt(id))).thenReturn(thr);
            assertEquals(thr, controller.CheckIdOrSlug(id), "Thread found by ID");
        }
    }

    @Test
    @DisplayName("Check thread by slug")
    void checkThreadBySlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
            assertEquals(thr, controller.CheckIdOrSlug(slug), "Thread found by slug");
        }
    }

    @Test
    @DisplayName("Post create")
    void create() {
        List<Post> poststhr = Collections.emptyList();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(poststhr),
                    controller.createPost(slug, poststhr));
        }
    }

    @Test
    @DisplayName("Post get")
    void post() {
        List<Post> poststhr = Collections.emptyList();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getPosts(thr.getId(), 200, 2, null, false)).thenReturn(poststhr);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(poststhr),
                    controller.Posts(slug, 200, 2, null, false));
        }
    }

    @Test
    @DisplayName("Thread change")
    void change() {
        Thread changeThread = new Thread("person", new Timestamp(2), "frm", "super msg", "changeSlug", "tpc", 4243);
        changeThread.setId(2);
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("changeSlug")).thenReturn(changeThread);
            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.change("changeSlug", thr),
                    "Change succesfull");
        }
    }

    @Test
    @DisplayName("Thread infrmation")
    void info() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.info(slug),
                    "Information succesfull");
        }
    }

    @Test
    @DisplayName("Vote create")
    void vote() {
        Vote vote = new Vote("person", 5);
        User user = new User("person", "g.succi@innopolus.ru", "Giancarlo Succi", "Succi");
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                ThreadController controller = new ThreadController();

                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
                userMock.when(() -> UserDAO.Info("person")).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.createVote(slug, vote),
                        "Successfull");
            }
        }
    }
}
